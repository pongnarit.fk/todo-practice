import {
  Container,
  Input,
  Box,
  Flex,
  Button,
  Card,
  Spacer,
  Checkbox,
  Stack,
  Text,
  SimpleGrid,
} from "@chakra-ui/react";
import { useMemo, useState } from "react";
import { FaRegEdit } from "react-icons/fa";
import { MdDeleteOutline } from "react-icons/md";
import { LiaTrashRestoreAltSolid } from "react-icons/lia";
import EditInputForm from "./components/EditForm";

function App() {
  const [list, setList] = useState([]);
  const [input, setInput] = useState("");

  const listNotDelete = useMemo(() => {
    return list.filter((item) => item.isDeleted === false);
  }, [list]);

  const listDelete = useMemo(() => {
    return list.filter((item) => item.isDeleted === true);
  }, [list]);

  const todoCompleted = useMemo(() => {
    return list.filter(
      (item) => item.isDone === true && item.isDeleted !== true
    );
  }, [list]);

  const onClickAdd = () => {
    const item = {
      id: Math.random(),
      data: input,
      isDone: false,
      isEdit: false,
      isDeleted: false,
    };
    setList([...list, item]);
    setInput("");
  };

  const onChangeStatus = (id, newStatus) => {
    const updateDaata = list.map((item) => {
      if (item.id === id) return { ...item, isDone: newStatus };
      return item;
    });
    setList(updateDaata);
  };

  const onClickDelete = (id, value) => {
    const newList = list.map((item) => {
      if (item.id === id) return { ...item, isDeleted: value };
      return item;
    });
    setList(newList);
  };

  const onClickEdit = (id, value) => {
    const newList = list.map((item) => {
      if (item.id === id) return { ...item, isEdit: value };
      return item;
    });
    setList(newList);
  };

  const onUpdateTask = (id, newTask) => {
    const newList = list.map((item) => {
      if (item.id === id) return { ...item, data: newTask, isEdit: false };
      return item;
    });
    setList(newList);
  };

  return (
    <Box w="100%" bg="#D6D6D6">
      <Container maxW="container.sm">
        <Box my="20">
          <Flex gap={2}>
            <Input
              value={input}
              onChange={(e) => setInput(e.target.value)}
              placeholder="What is the task today?"
            />
            <Button
              onClick={() => onClickAdd()}
              isDisabled={input ? false : true}
            >
              Add Task
            </Button>
          </Flex>
        </Box>
        <SimpleGrid columns={2} spacing={10}>
          <Box>
            <Text align="end">
              {todoCompleted.length}/{listNotDelete.length} Todos completed
            </Text>
            <Stack w="100%">
              {listNotDelete.map((item, index) => (
                <Card px="4" py="8" key={index}>
                  {item.isEdit ? (
                    <EditInputForm
                      id={item.id}
                      onUpdateTask={onUpdateTask}
                      onClickEdit={onClickEdit}
                    />
                  ) : (
                    <Flex w="100%" justify="space-bewteen" align="center">
                      <Checkbox
                        mr="2"
                        isChecked={item.isDone}
                        onChange={(e) =>
                          onChangeStatus(item.id, e.target.checked)
                        }
                      />
                      <Text as={item.isDone ? "del" : ""}>{item.data}</Text>
                      <Spacer />
                      <Flex gap={2}>
                        <Button
                          variant="ghost"
                          onClick={() => onClickEdit(item.id, true)}
                        >
                          <FaRegEdit />
                        </Button>
                        <Button
                          variant="ghost"
                          onClick={() => onClickDelete(item.id, true)}
                        >
                          <MdDeleteOutline color="red" />
                        </Button>
                      </Flex>
                    </Flex>
                  )}
                </Card>
              ))}
            </Stack>
          </Box>
          <Box>
            <Text align="end">{listDelete.length} Deleted</Text>
            <Stack w="100%">
              {listDelete.map((item, index) => (
                <Card px="4" py="8" key={index}>
                  <Flex w="100%" justify="space-bewteen" align="center">
                    <Checkbox
                      mr="2"
                      isDisabled
                      isChecked={item.isDone}
                      onChange={(e) =>
                        onChangeStatus(item.id, e.target.checked)
                      }
                    />
                    <Text as={item.isDone ? "del" : ""}>{item.data}</Text>
                    <Spacer />
                    <Button
                      variant="ghost"
                      onClick={() => onClickDelete(item.id, false)}
                    >
                      <LiaTrashRestoreAltSolid />
                    </Button>
                  </Flex>
                </Card>
              ))}
            </Stack>
          </Box>
        </SimpleGrid>
      </Container>
    </Box>
  );
}

export default App;
