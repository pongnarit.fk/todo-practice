import { useState } from "react";
import { Input, Flex, Button, Stack, Spacer } from "@chakra-ui/react";

// eslint-disable-next-line react/prop-types
export default function EditInputForm({ id, onUpdateTask, onClickEdit }) {
  const [edit, setEdit] = useState("");
  return (
    <Stack gap={2}>
      <Input
        value={edit}
        onChange={(e) => setEdit(e.target.value)}
        placeholder="Update task"
      />
      <Flex gap={2}>
        <Button onClick={() => onClickEdit(id, false)}>Cancel</Button>
        <Spacer />
        <Button
          onClick={() => onUpdateTask(id, edit)}
          isDisabled={edit ? false : true}
        >
          Update Task
        </Button>
      </Flex>
    </Stack>
  );
}
